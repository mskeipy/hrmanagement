﻿using HRM.Model.Model;
using HRM.Reponsitory.Shared;
using HRM.UnitOfWork.Reponsitory;
using HRM.UnitOfWork.Shared;
using HRM.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace HRM.Reponsitory.Reponsitory
{
    public class UserReponsitory : GenericReponsitory<User>, IGenericReponsitory<User>, IUserReponsitory
    {
        public UserReponsitory(DbContext dbContext) : base(dbContext)
        {
        }
    }
}