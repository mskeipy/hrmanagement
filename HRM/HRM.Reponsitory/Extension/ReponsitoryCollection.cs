﻿using HRM.Reponsitory.Reponsitory;
using HRM.Reponsitory.Shared;
using Microsoft.Extensions.DependencyInjection;

namespace HRM.Reponsitory.Extension
{
    public static class ReponsitoryCollection
    {
        public static IServiceCollection AddRepositoriesInstance(this IServiceCollection service)
        {
            service.AddTransient<IUserReponsitory, UserReponsitory>();
            
            return service;
        }
    }
}