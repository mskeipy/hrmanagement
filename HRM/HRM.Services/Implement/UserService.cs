﻿using HRM.Model.Model;
using HRM.Services.Collection;
using HRM.Services.Shared;
using HRM.UnitOfWork.Shared;
using HRM.Models.Models;

namespace HRM.Services.Implement
{
    public class UserService : BaseService<User>, IBaseService<User>, IUserService
    {
        IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}