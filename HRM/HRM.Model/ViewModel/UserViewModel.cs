﻿using System.ComponentModel.DataAnnotations;

namespace HRM.Models.Models.ViewModel
{
    public class UserViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}