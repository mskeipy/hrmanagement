﻿using AutoMapper;
using HRM.Model.Model;
using HRM.Models.Models.ViewModel;

namespace HRM.Models.Models
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserViewModel>().ReverseMap();
        }
    }
}