﻿using HRM.Model.Model;
using Microsoft.EntityFrameworkCore;

namespace HRM.Models.Models
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<BaseEntity> BaseEntities { get; set; }

    }
}
