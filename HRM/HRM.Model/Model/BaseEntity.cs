﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HRM.Model.Model
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }

    public class BaseEntity : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        public Guid? CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? LastModified { get; set; }
        public Guid? OwnerId { get; set; }
        public int? isDelete { get; set; }
        public int? Flags { get; set; }
    }
}