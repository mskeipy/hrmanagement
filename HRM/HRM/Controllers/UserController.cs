﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using HRM.Model.Model;
using HRM.Services.Shared;
using HRM.Models.Models;

using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        IUserService _userService;
        private IMapper _mapper;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/Space
        [HttpGet]
        public async Task<object> Get()
        {
            var users = await _userService.GetAllAsync();
            return users;
        }

        // GET: api/Space/5                 
        [HttpGet("user/{id}")]
        public User Get(Guid id)
        {
            var users = _userService.Get(id);
            return users;
        }

        // POST: api/Space
        [HttpPost]
        public User Post([FromBody] User users)
        {
            var s = _userService.Create(users);
            return s;
        }

        // PUT: api/Space/5
        [HttpPut("{id}")]
        public User Put(Guid id, [FromBody] User users)
        {
            users.Id = id;
            var s = _userService.Update(users);
            return s;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public User Delete(Guid id)
        {
            var c = _userService.Delete(id);
            return c;
        }
        
    }
}